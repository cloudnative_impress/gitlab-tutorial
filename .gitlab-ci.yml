---
# GitLab Practical Guide
# To contribute improvements to CI/CD, please follow the tutorial guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html

default:
  image: docker.io/openjdk:17

variables:
  BUILD_HOME: ${CI_PROJECT_DIR}/build
  MANIFESTS_HOME: ${CI_PROJECT_DIR}/deploy
  FF_USE_NEW_SHELL_ESCAPE: "True"
  FF_USE_FASTZIP: "True"

stages:
  - build
  - test
  - package
  - development
  - staging
  - production
  - release
    
include:
  - template: Security/Container-Scanning.gitlab-ci.yml
  - local: .gitlab/ci/template-deployment.yml

quarkus-build:
  stage: build
  image:
  variables:
    MAVEN_OPTS: >-
     -Dhttps.protocols=TLSv1.2
     -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
     -Dmaven.test.skip=true
     -Dorg.slf4j.simpleLogger.showDateTime=true
     -Djava.awt.headless=true
  script:
    - cd ${BUILD_HOME}
    - ./mvnw package
  artifacts:
    paths:
      - ${BUILD_HOME}/target/quarkus-app/
    expire_in: 30 mins

quarkus-test:
  stage: test
  dependencies: []
  script:
    - cd ${BUILD_HOME}
    - ./mvnw test
  artifacts:
    reports:
      junit:
        - ${BUILD_HOME}/target/surefire-reports/TEST-*.xml

quarkus-container-package:
  stage: package
  image:
    name: quay.io/buildah/stable
  dependencies:
    - quarkus-build
  variables:
    BUILDAH_ISOLATION: "rootless"
  before_script:
    - buildah login --username "${CI_REGISTRY_USER}"
        --password "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
  script:
    - cd ${BUILD_HOME}
    - buildah bud -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" .
    - buildah push --digestfile="./container-digest.env"
        "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
        docker://${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
    ## Making a environment value of the pushed container digest.
    - sed -ie '1s/^/CI_IMAGE_DIGEST=/g' ${BUILD_HOME}/container-digest.env
    - cat ${BUILD_HOME}/container-digest.env
  after_script:
    - buildah logout "${CI_REGISTRY}"
  artifacts:
    reports:
      dotenv: ${BUILD_HOME}/container-digest.env

## https://docs.gitlab.com/ee/user/application_security/container_scanning/
container_scanning:
  stage: development
  dependencies: []
  variables:
    CS_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    CS_ANALYZER_IMAGE: registry.gitlab.com/security-products/container-scanning/trivy:6

deploy-dev:
  stage: development
  extends:
    - .deploy
  environment:
    name: review/${CI_COMMIT_REF_SLUG}
    url: http://${CI_ENVIRONMENT_SLUG}-quarkus-app.example.com/
    on_stop: stop-dev
    deployment_tier: development
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /^feature(\/|-)/

stop-dev:
  stage: development
  extends:
    - .deploy
  when: manual
  script:
    - kubectl delete namespace ${CI_ENVIRONMENT_SLUG}
  environment:
    name: review/${CI_COMMIT_REF_SLUG}
    url: http://${CI_ENVIRONMENT_SLUG}-quarkus-app.example.com
    action: stop
    deployment_tier: development
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      when: never
    - if: $CI_COMMIT_BRANCH =~ /^feature(\/|-)/

deploy-stg:
  stage: staging
  extends:
    - .deploy
  environment:
    name: stg
    url: http://stg-quarkus-app.example.com
    deployment_tier: staging
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
    - if: $CI_PIPELINE_SOURCE != "merge_request_event"
      when: never

deploy-prod:
  stage: production
  extends:
    - .deploy
  when: manual
  environment:
    name: prod
    url: http://quarkus-app.example.com
    deployment_tier: production
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      when: manual
    - if: $CI_PIPELINE_SOURCE != "merge_request_event"
      when: never

release:
  stage: release
  dependencies: []
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  variables:
    CHANGELOG: https://gitlab.com/$CI_PROJECT_PATH/blob/$CI_COMMIT_TAG/CHANGELOG.md
  environment:
    name: stable/quarkus-app
    url: https://gitlab.com/$CI_PROJECT_PATH/-/releases
  script:
    - echo "Latest release Quarkus Application for GitLab Tutorial"
  release:
    name: '$CI_COMMIT_TAG'
    description: |
      See [the changelog]($CHANGELOG) :rocket:

      Quarkus documentation can be found at https://quarkus.io/guides/getting-started.
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'
  rules:
    - if: $CI_COMMIT_TAG 
